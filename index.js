console.log('Hello world');
let users = ['Dwayne Johnson','Steve Austin','Kurt Angle','Dave Boutista'];
console.log('Original Array');
console.log(users);

function addUser(a){
	
	users.length++;
	users[users.length-1] = a;
}
let username = "John Cena";
addUser(username);
console.log(users);

function printItem(i){
	
	return users[i];
}
let index = 2;
let itemFound = printItem(index);
console.log(itemFound);



function deleteLastItem(){
	let lastItem = users[users.length-1];
	users.length--;
	return lastItem;
}
let last = deleteLastItem();
console.log(last);
console.log(users);

function updateElement(x,y){
	users[y] = x; 
}

updateElement("Triple H",3);

console.log(users);

function deleteAllItems(){
	while(users.length>0){
		users.length--;
	}
}
deleteAllItems();

console.log(users);

function checkIfEmpty(){
	if(users.length>0){
		return false;
	}
	else return true;
}
let isUsersEmpty = checkIfEmpty();
console.log(isUsersEmpty);